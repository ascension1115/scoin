//Server.js, don't forget to add express & ejs to packages
const express = require('express')
const app = express()
const port = 3004; // process.env.PORT || 3003
const router = express.Router()
const cors = require('cors');
const bodyParser = require('body-parser');
const path = require('path');

const DistPath = path.normalize( `${ __dirname }/../spa/dist` );
console.log( DistPath );

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(express.static(DistPath)) // set the static files location for the static html
app.engine('.html', require('ejs').renderFile)

app.set('views', DistPath)

app.post('/signup', function(req, res, next){

    console.log('Sign up requested with...', req.body);
    res.send(200);
    
});

app.use('/', router)

router.get('/*', (req, res, next) => {
  res.sendFile(`${ DistPath }/index.html`)
})

app.listen(port)
console.log('App running on port', port)